require("functions")

local function GetAttributes(data)
	local list = {}

	for k,v in pairs(data) do
		if type(k) ~= "number" then
			table.insert(list, {k = k, v = v})
		end
	end

	return list
end

local Linq = require("Linq")

local test1 = 
{
	Attribute1 = "Attribute1",
	Attribute2 = "Attribute2",
	Attribute3 = "Attribute3",
}

print(string.format("{%s}", table.concat(Linq:Linq(GetAttributes(test1)):Select(function(n) return string.format("%s:%s", n.k, n.v) end), ", ")))

local test2 = 
{
	{
		id = 1,
		name = "test2_1",
		value = "test2_1",
	},	
	{
		id = 2,
		name = "test2_2",
		value = "test2_2",
	},	
	{
		id = 3,
		name = "test2_3",
		value = "test2_3",
	},	
	{
		id = 4,
		name = "test2_4",
		value = "test2_4",
	},	
	{
		id = 5,
		name = "test2_5",
		value = "test2_5",
	},	
	{
		id = 6,
		name = "test2_6",
		value = "test2_6",
	},	
}

print(table.concat(Linq:Linq(test2):Select(function(n) return n.id end), ", "))

local temp = Linq:Linq(test2):Where(function(n) return n.id % 2 == 1 end)
for i, item in ipairs(temp) do
	print(string.format("{%s}", table.concat(Linq:Linq(GetAttributes(item)):Select(function(n) return string.format("%s:%s", n.k, n.v) end), ", ")))
end


local test3 = 
{
	key1 = "key1",	
	key2 = "key2",	
	key3 = "key3",	
	key4 = "key4",	
}

local keys1 = {"key1", "key2", "key3"}
local keys2 = {"key1", "key2", "key5"}
local temp1 = {}
local all = Linq:Linq(keys1):All(function(n)
	local item = test3[n]

	if item then
		temp1[n] = item
	end

	return item ~= nil
end)

if all then
	print(string.format("key1 {%s}", table.concat(Linq:Linq(GetAttributes(temp1)):Select(function(n) return string.format("%s:%s", n.k, n.v) end), ", ")))
else
	print("key1 Error")
end

local all = Linq:Linq(keys2):All(function(n)
	local item = test3[n]

	if item then
		temp1[n] = item
	end

	return item ~= nil
end)

if all then
	print(string.format("key2 {%s}", table.concat(Linq:Linq(GetAttributes(temp1)):Select(function(n) return string.format("%s:%s", n.k, n.v) end), ", ")))
else
	print("key2 Error")
end




