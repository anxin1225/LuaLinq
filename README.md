#让Lua支持Linq吧
第一次接触Linq是在使用C#的时候，这种语法，在处理列表数据非常方便。如果想了解Linq的更多内容可以百度一下Linq，不过你不了解也没关系，让我在Lua中给你展示一下Linq的魅力。简单点说，Linq就是让忽略掉循环的部分，让你更加专业相关的业务实现。算是一种语法糖。

##简单举几个例子

###1、输出一个类中的所有内容

~~~
local test1 = 
{
	Attribute1 = "Attribute1",
	Attribute2 = "Attribute2",
	Attribute3 = "Attribute3",
}

print(string.format("{%s}", table.concat(Linq:Linq(GetAttributes(test1)):Select(function(n) return string.format("%s:%s", n.k, n.v) end), ", ")))
~~~
输出：

~~~
{Attribute2:Attribute2, Attribute1:Attribute1, Attribute3:Attribute3}
~~~

###2、提取列表数据中的Id组装成一个新的Id列表

~~~
local test2 = 
{
	{
		id = 1,
		name = "test2_1",
		value = "test2_1",
	},	
	{
		id = 2,
		name = "test2_2",
		value = "test2_2",
	},	
	{
		id = 3,
		name = "test2_3",
		value = "test2_3",
	},	
	{
		id = 4,
		name = "test2_4",
		value = "test2_4",
	},	
	{
		id = 5,
		name = "test2_5",
		value = "test2_5",
	},	
	{
		id = 6,
		name = "test2_6",
		value = "test2_6",
	},	
}

print(table.concat(Linq:Linq(test2):Select(function(n) return n.id end), ", "))
~~~
输出：

~~~
1, 2, 3, 4, 5, 6
~~~

###3、提取列表中id为单数的内容，组装成一个新的列表

~~~
local temp = Linq:Linq(test2):Where(function(n) return n.id % 2 == 1 end)
for i, item in ipairs(temp) do
	print(string.format("{%s}", table.concat(Linq:Linq(GetAttributes(item)):Select(function(n) return string.format("%s:%s", n.k, n.v) end), ", ")))
end
~~~
输出：

~~~
{id:1, name:test2_1, value:test2_1}
{id:3, name:test2_3, value:test2_3}
{id:5, name:test2_5, value:test2_5}
~~~

###4、从字典中组装一个table出来，如果所有的Key都存在则返回数据，否则则返回nil

~~~
local test3 = 
{
	key1 = "key1",	
	key2 = "key2",	
	key3 = "key3",	
	key4 = "key4",	
}

local keys1 = {"key1", "key2", "key3"}
local keys2 = {"key1", "key2", "key5"}
local temp1 = {}
local all = Linq:Linq(keys1):All(function(n)
	local item = test3[n]

	if item then
		temp1[n] = item
	end

	return item ~= nil
end)

if all then
	print(string.format("key1 {%s}", table.concat(Linq:Linq(GetAttributes(temp1)):Select(function(n) return string.format("%s:%s", n.k, n.v) end), ", ")))
else
	print("key1 Error")
end

local all = Linq:Linq(keys2):All(function(n)
	local item = test3[n]

	if item then
		temp1[n] = item
	end

	return item ~= nil
end)

if all then
	print(string.format("key2 {%s}", table.concat(Linq:Linq(GetAttributes(temp1)):Select(function(n) return string.format("%s:%s", n.k, n.v) end), ", ")))
else
	print("key2 Error")
end
~~~

输出：

~~~
key1 {key1:key1, key3:key3, key2:key2}
key2 Error
~~~

##综述
Linq的用处还有很多，需要你发挥想象力来使用它，这个地方我就不做过多的赘述了。下面我来简单的说明一下能够使用的方法。
###能用的函数
####Linq:Linq
传入一个列表类型的table，以后的操作都以列表类型为基础进行操作。方法内容会将传过来的对象Clone一份，不会破坏原来的数据。So放心大胆的用，虽然，Linq支持将列表类传递进来使用，但是我依然不推荐你这么用。
####Select
这个函数会创建一个新的添加了Linq代码的列表。你在调用这个函数的时候需要传入一个函数，Linq会将列表中的每一项数据作为参数传入该函，这个函数的返回值会插入到新的列表中返回。简单点说。这个函数就是将原来的列表对象转换为新的列表对象的方法。
####Where
筛选函数。调用该函数时，需要传入一个函数，Linq会吧列表中的数据，依次传入该函数，并将该函数的返回值作为bool值判定，是否将这一对象添加到新列表中并返回。
####Sum
求和函数，调用该函数时，需要闯入一个函数，Linq会把了你报中的数据，依次传入该函数，并且将该函数的返回值累加起来返回。
####All
判定函数，判定列表对象中所有的对象，是否都通过了校验。调用这个函数的时候，需要传入一个校验函数，Linq会将列表中的每一项传入校验函数，并且接受校验结果，如果任何一项判定失败，则直接返回失败，否则则返回成功。
####Any
判定函数，判定列表对象中任何一个对象是否通过了校验。调用这个函数的时候，需要传入一个校验函数，Linq会将列表中的每一项以此传入校验函数，如果任何一项的校验结果判定成功，则直接返回成功，否则则返回失败。
####First
获取函数，获取数据的第一项。调用这个函数时，可以传入一个校验函数，如果存在校验函数则以校验成功的第一项作为结果返回。
####Get
获取函数，获取某一个具体对象的数据。此函数的针对对象不是列表，而是某一个校验对象。获取校验对象的名称属性。
####Sort
排序函数，传入一个排序函数，这个函数接受两个值。该函数会将排好序的数据返回。
####Clear
清理函数，清理数据中的Linq方法


##疑问
###1、使用Linq会不会破坏原来的数据？
不会，因为所有的数据都是从新Clone了一次，是操作Clone之后的数据，所以不会对原来的数据造成影响
###2、使用Linq会不会降低性能？
会，但是影响应该不大。影响性能的主要因素主要有这么几个：1、Clone，确实会影响一些，不过为了数据安全，这样做还是很有必要的，再说使用Linq的时候一般是用来处理数据初始化，所以性能低一些也没什么关系；2、列表的循环，我想这个应该不能算作是主要原因，因为你要做列表处理，肯定要使用循环，都有循环，所以这个没什么关系;

##最后
Linq只是一种处理列表数据的思想，使用这种数据处理列表会变得相对简单，只关心直接相关的业务逻辑即可。书写起来也比较方便。总之我比较喜欢这种方式。#LuaLinq

